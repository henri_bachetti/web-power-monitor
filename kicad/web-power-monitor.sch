EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:my-power-supplies
LIBS:my-transistors
LIBS:my-batteries
LIBS:my-power-monitors
LIBS:my-power
LIBS:Raspberry_Pi_2_or_3
LIBS:my-microcontrollers
LIBS:my-regulators
LIBS:my-connectors
LIBS:web-power-monitor-cache
EELAYER 25 0
EELAYER END
$Descr A3 11693 16535 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ina226-module U2
U 1 1 5E15EFED
P 7150 4750
F 0 "U2" H 7200 5100 60  0000 C CNN
F 1 "ina226-module" H 7200 5400 60  0000 C CNN
F 2 "myModules:CJMCU-ina226" H 7150 4750 60  0001 C CNN
F 3 "" H 7150 4750 60  0000 C CNN
	1    7150 4750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5E15F64E
P 6650 4800
F 0 "#PWR01" H 6650 4550 50  0001 C CNN
F 1 "GND" H 6650 4650 50  0000 C CNN
F 2 "" H 6650 4800 50  0000 C CNN
F 3 "" H 6650 4800 50  0000 C CNN
	1    6650 4800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X20 P3
U 1 1 5E2471FC
P 8600 6550
F 0 "P3" H 8600 7600 50  0000 C CNN
F 1 "IO" V 8700 6550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20" H 8600 6550 50  0001 C CNN
F 3 "" H 8600 6550 50  0000 C CNN
	1    8600 6550
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR02
U 1 1 5E247398
P 7850 4600
F 0 "#PWR02" H 7850 4450 50  0001 C CNN
F 1 "+3.3V" H 7850 4740 50  0000 C CNN
F 2 "" H 7850 4600 50  0000 C CNN
F 3 "" H 7850 4600 50  0000 C CNN
	1    7850 4600
	1    0    0    -1  
$EndComp
$Comp
L ESP32-WROOM U1
U 1 1 5E247E2B
P 4850 6850
F 0 "U1" H 4150 8100 60  0000 C CNN
F 1 "ESP32-WROOM" H 5350 8100 60  0000 C CNN
F 2 "myMicroControllers:ESP32-WROOM-32" H 5200 8200 60  0001 C CNN
F 3 "" H 4400 7300 60  0001 C CNN
	1    4850 6850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5E247E39
P 4400 8700
F 0 "#PWR03" H 4400 8450 50  0001 C CNN
F 1 "GND" H 4400 8550 50  0000 C CNN
F 2 "" H 4400 8700 50  0000 C CNN
F 3 "" H 4400 8700 50  0000 C CNN
	1    4400 8700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5E247E3F
P 3200 7700
F 0 "#PWR04" H 3200 7450 50  0001 C CNN
F 1 "GND" H 3200 7550 50  0000 C CNN
F 2 "" H 3200 7700 50  0000 C CNN
F 3 "" H 3200 7700 50  0000 C CNN
	1    3200 7700
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR05
U 1 1 5E247E45
P 3200 6100
F 0 "#PWR05" H 3200 5950 50  0001 C CNN
F 1 "+3V3" H 3200 6240 50  0000 C CNN
F 2 "" H 3200 6100 50  0000 C CNN
F 3 "" H 3200 6100 50  0000 C CNN
	1    3200 6100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5E247E4B
P 4700 5000
F 0 "#PWR06" H 4700 4750 50  0001 C CNN
F 1 "GND" H 4700 4850 50  0000 C CNN
F 2 "" H 4700 5000 50  0000 C CNN
F 3 "" H 4700 5000 50  0000 C CNN
	1    4700 5000
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 5E247E51
P 2600 7700
F 0 "SW1" H 2750 7810 50  0000 C CNN
F 1 "EN" H 2600 7620 50  0000 C CNN
F 2 "mySwitches:SW_PUSH_6mm" H 2600 7700 50  0001 C CNN
F 3 "" H 2600 7700 50  0000 C CNN
	1    2600 7700
	0    1    1    0   
$EndComp
$Comp
L GND #PWR07
U 1 1 5E247E58
P 2450 8200
F 0 "#PWR07" H 2450 7950 50  0001 C CNN
F 1 "GND" H 2450 8050 50  0000 C CNN
F 2 "" H 2450 8200 50  0000 C CNN
F 3 "" H 2450 8200 50  0000 C CNN
	1    2450 8200
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW2
U 1 1 5E247E5E
P 6050 9100
F 0 "SW2" H 6200 9210 50  0000 C CNN
F 1 "BOOT" H 6050 9020 50  0000 C CNN
F 2 "mySwitches:SW_PUSH_6mm" H 6050 9100 50  0001 C CNN
F 3 "" H 6050 9100 50  0000 C CNN
	1    6050 9100
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 5E247E65
P 2600 6050
F 0 "R1" V 2680 6050 50  0000 C CNN
F 1 "10K" V 2600 6050 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2530 6050 50  0001 C CNN
F 3 "" H 2600 6050 50  0000 C CNN
	1    2600 6050
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR08
U 1 1 5E247E6C
P 2600 5800
F 0 "#PWR08" H 2600 5650 50  0001 C CNN
F 1 "+3V3" H 2600 5940 50  0000 C CNN
F 2 "" H 2600 5800 50  0000 C CNN
F 3 "" H 2600 5800 50  0000 C CNN
	1    2600 5800
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5E247E72
P 2300 7700
F 0 "C1" H 2325 7800 50  0000 L CNN
F 1 "100nF" H 2050 7600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 2338 7550 50  0001 C CNN
F 3 "" H 2300 7700 50  0000 C CNN
	1    2300 7700
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5E247E79
P 6400 9100
F 0 "C4" H 6425 9200 50  0000 L CNN
F 1 "100nF" H 6450 9000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 6438 8950 50  0001 C CNN
F 3 "" H 6400 9100 50  0000 C CNN
	1    6400 9100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5E247E80
P 6250 9600
F 0 "#PWR09" H 6250 9350 50  0001 C CNN
F 1 "GND" H 6250 9450 50  0000 C CNN
F 2 "" H 6250 9600 50  0000 C CNN
F 3 "" H 6250 9600 50  0000 C CNN
	1    6250 9600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5E247E8B
P 5900 7600
F 0 "#PWR010" H 5900 7350 50  0001 C CNN
F 1 "GND" H 5900 7450 50  0000 C CNN
F 2 "" H 5900 7600 50  0000 C CNN
F 3 "" H 5900 7600 50  0000 C CNN
	1    5900 7600
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5E247EC7
P 3200 6600
F 0 "C3" H 3225 6700 50  0000 L CNN
F 1 "100nF" H 3100 6450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 3238 6450 50  0001 C CNN
F 3 "" H 3200 6600 50  0000 C CNN
	1    3200 6600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5E247ECE
P 3200 7150
F 0 "#PWR011" H 3200 6900 50  0001 C CNN
F 1 "GND" H 3200 7000 50  0000 C CNN
F 2 "" H 3200 7150 50  0000 C CNN
F 3 "" H 3200 7150 50  0000 C CNN
	1    3200 7150
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 5E247ED7
P 2950 6600
F 0 "C2" H 2975 6700 50  0000 L CNN
F 1 "10µF" H 2900 6500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 2988 6450 50  0001 C CNN
F 3 "" H 2950 6600 50  0000 C CNN
	1    2950 6600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P2
U 1 1 5E24A7B3
P 8600 4450
F 0 "P2" H 8600 4700 50  0000 C CNN
F 1 "POWER" V 8700 4450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 8600 4450 50  0001 C CNN
F 3 "" H 8600 4450 50  0000 C CNN
	1    8600 4450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5E24A896
P 8300 7600
F 0 "#PWR012" H 8300 7350 50  0001 C CNN
F 1 "GND" H 8300 7450 50  0000 C CNN
F 2 "" H 8300 7600 50  0000 C CNN
F 3 "" H 8300 7600 50  0000 C CNN
	1    8300 7600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5E24A96F
P 8300 4700
F 0 "#PWR013" H 8300 4450 50  0001 C CNN
F 1 "GND" H 8300 4550 50  0000 C CNN
F 2 "" H 8300 4700 50  0000 C CNN
F 3 "" H 8300 4700 50  0000 C CNN
	1    8300 4700
	1    0    0    -1  
$EndComp
$Comp
L LM3940 U4
U 1 1 5E24B3EA
P 3400 9250
F 0 "U4" H 3550 9055 60  0000 C CNN
F 1 "LM3940" H 3400 9450 60  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical" H 3400 9250 60  0001 C CNN
F 3 "" H 3400 9250 60  0000 C CNN
	1    3400 9250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 5E24B4F7
P 2700 9500
F 0 "#PWR014" H 2700 9250 50  0001 C CNN
F 1 "GND" H 2700 9350 50  0000 C CNN
F 2 "" H 2700 9500 50  0000 C CNN
F 3 "" H 2700 9500 50  0000 C CNN
	1    2700 9500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5E24B5E9
P 3400 9600
F 0 "#PWR015" H 3400 9350 50  0001 C CNN
F 1 "GND" H 3400 9450 50  0000 C CNN
F 2 "" H 3400 9600 50  0000 C CNN
F 3 "" H 3400 9600 50  0000 C CNN
	1    3400 9600
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR016
U 1 1 5E24B6DD
P 3900 9100
F 0 "#PWR016" H 3900 8950 50  0001 C CNN
F 1 "+3V3" H 3900 9240 50  0000 C CNN
F 2 "" H 3900 9100 50  0000 C CNN
F 3 "" H 3900 9100 50  0000 C CNN
	1    3900 9100
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5E25B318
P 7850 4950
F 0 "C5" H 7875 5050 50  0000 L CNN
F 1 "100nF" H 7900 4850 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 7888 4800 50  0001 C CNN
F 3 "" H 7850 4950 50  0000 C CNN
	1    7850 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 5E25B48B
P 7850 5200
F 0 "#PWR017" H 7850 4950 50  0001 C CNN
F 1 "GND" H 7850 5050 50  0000 C CNN
F 2 "" H 7850 5200 50  0000 C CNN
F 3 "" H 7850 5200 50  0000 C CNN
	1    7850 5200
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P1
U 1 1 5E25E283
P 4800 4700
F 0 "P1" H 4800 4900 50  0000 C CNN
F 1 "FTDI" V 4900 4700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 4800 4700 50  0001 C CNN
F 3 "" H 4800 4700 50  0000 C CNN
	1    4800 4700
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 5E26EF82
P 6250 7650
F 0 "R2" V 6330 7650 50  0000 C CNN
F 1 "10K" V 6250 7650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 6180 7650 50  0001 C CNN
F 3 "" H 6250 7650 50  0000 C CNN
	1    6250 7650
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR018
U 1 1 5E26F0D6
P 6250 7400
F 0 "#PWR018" H 6250 7250 50  0001 C CNN
F 1 "+3V3" H 6250 7540 50  0000 C CNN
F 2 "" H 6250 7400 50  0000 C CNN
F 3 "" H 6250 7400 50  0000 C CNN
	1    6250 7400
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5E26F477
P 2900 9450
F 0 "C6" H 2925 9550 50  0000 L CNN
F 1 "470nF" H 2950 9350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2_P5" H 2938 9300 50  0001 C CNN
F 3 "" H 2900 9450 50  0000 C CNN
	1    2900 9450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5E26F667
P 2900 9700
F 0 "#PWR019" H 2900 9450 50  0001 C CNN
F 1 "GND" H 2900 9550 50  0000 C CNN
F 2 "" H 2900 9700 50  0000 C CNN
F 3 "" H 2900 9700 50  0000 C CNN
	1    2900 9700
	1    0    0    -1  
$EndComp
$Comp
L CP C7
U 1 1 5E26F7C1
P 3900 9450
F 0 "C7" H 3925 9550 50  0000 L CNN
F 1 "33µF" H 3850 9350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Radial_D5_L11_P2.5" H 3938 9300 50  0001 C CNN
F 3 "" H 3900 9450 50  0000 C CNN
	1    3900 9450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 5E26F936
P 3900 9700
F 0 "#PWR020" H 3900 9450 50  0001 C CNN
F 1 "GND" H 3900 9550 50  0000 C CNN
F 2 "" H 3900 9700 50  0000 C CNN
F 3 "" H 3900 9700 50  0000 C CNN
	1    3900 9700
	1    0    0    -1  
$EndComp
Text GLabel 5300 5100 1    60   Input ~ 0
TX
Text GLabel 5450 5100 1    60   Input ~ 0
RX
$Comp
L Barrel_Jack_MountingPin P5
U 1 1 5E2C77F7
P 2300 9300
F 0 "P5" H 2300 9510 50  0000 C CNN
F 1 "JACK" H 2350 9050 50  0000 L CNN
F 2 "myConnectors:LUNBERG-1613-21" H 2350 9260 50  0001 C CNN
F 3 "" H 2350 9260 50  0001 C CNN
	1    2300 9300
	1    0    0    -1  
$EndComp
Text GLabel 7700 3850 1    60   Input ~ 0
V+
Text GLabel 7850 3850 1    60   Input ~ 0
V-
Text GLabel 7550 3850 1    60   Input ~ 0
V+
Wire Wire Line
	6600 4500 6750 4500
Wire Wire Line
	6750 4300 6650 4300
Wire Wire Line
	6750 4700 6650 4700
Wire Wire Line
	6650 4700 6650 4800
Wire Wire Line
	7650 4300 8400 4300
Wire Wire Line
	7850 4600 7850 4800
Wire Wire Line
	7850 4700 7650 4700
Wire Wire Line
	5750 6200 6400 6200
Wire Wire Line
	6400 6200 6400 5400
Wire Wire Line
	7300 5450 6450 5450
Wire Wire Line
	6450 5450 6450 6500
Wire Wire Line
	6450 6500 5750 6500
Wire Wire Line
	7300 7000 7300 8250
Wire Wire Line
	7350 8300 7350 6900
Wire Wire Line
	7400 6800 7400 8350
Wire Wire Line
	7450 7200 7450 8400
Wire Wire Line
	7550 7400 7550 8500
Wire Wire Line
	7600 8550 7600 7300
Wire Wire Line
	5750 6400 6000 6400
Wire Wire Line
	6000 6400 6000 5200
Wire Wire Line
	6000 5200 4900 5200
Wire Wire Line
	5750 6300 5950 6300
Wire Wire Line
	5950 6300 5950 5250
Wire Wire Line
	5950 5250 4800 5250
Wire Wire Line
	4400 8700 4400 7900
Wire Wire Line
	3200 7700 3200 7550
Wire Wire Line
	2600 6350 3900 6350
Wire Wire Line
	3200 6100 3200 6450
Wire Wire Line
	2950 6250 3900 6250
Wire Wire Line
	4800 5250 4800 4900
Wire Wire Line
	4700 4900 4700 5000
Wire Wire Line
	5750 7300 6050 7300
Wire Wire Line
	2600 6200 2600 7400
Wire Wire Line
	2600 8100 2600 8000
Wire Wire Line
	6050 9400 6050 9500
Wire Wire Line
	2600 5800 2600 5900
Connection ~ 2600 6350
Wire Wire Line
	2300 7550 2300 7300
Wire Wire Line
	2300 7300 2600 7300
Connection ~ 2600 7300
Wire Wire Line
	2300 8100 2300 7850
Wire Wire Line
	6400 8950 6400 8700
Wire Wire Line
	6400 9500 6400 9250
Wire Wire Line
	6400 8700 6050 8700
Connection ~ 6050 8700
Wire Wire Line
	5900 7600 5900 7500
Wire Wire Line
	5900 7500 5750 7500
Wire Wire Line
	2300 8100 2600 8100
Wire Wire Line
	2450 8100 2450 8200
Connection ~ 2450 8100
Wire Wire Line
	6050 9500 6400 9500
Wire Wire Line
	6250 9500 6250 9600
Connection ~ 6250 9500
Wire Wire Line
	3200 6750 3200 7150
Connection ~ 3200 6250
Wire Wire Line
	2950 6450 2950 6250
Wire Wire Line
	3200 7050 2950 7050
Wire Wire Line
	2950 7050 2950 6750
Connection ~ 3200 7050
Wire Wire Line
	7050 8000 5300 8000
Wire Wire Line
	5300 8000 5300 7900
Wire Wire Line
	7200 8050 3800 8050
Wire Wire Line
	3800 8050 3800 7450
Wire Wire Line
	3800 7450 3900 7450
Wire Wire Line
	4500 8100 4500 7900
Wire Wire Line
	3750 8150 3750 7350
Wire Wire Line
	3750 7350 3900 7350
Wire Wire Line
	7100 8200 5200 8200
Wire Wire Line
	5200 8200 5200 7900
Wire Wire Line
	7300 8250 3700 8250
Wire Wire Line
	3700 8250 3700 7050
Wire Wire Line
	3700 7050 3900 7050
Wire Wire Line
	3900 7150 3650 7150
Wire Wire Line
	3650 7150 3650 8300
Wire Wire Line
	3650 8300 7350 8300
Wire Wire Line
	7400 8350 3600 8350
Wire Wire Line
	7450 8400 3550 8400
Wire Wire Line
	3200 7550 3900 7550
Wire Wire Line
	3900 7250 3600 7250
Wire Wire Line
	3600 7250 3600 8350
Wire Wire Line
	3550 8400 3550 6850
Wire Wire Line
	3550 6850 3900 6850
Wire Wire Line
	7550 8500 3450 8500
Wire Wire Line
	3450 8500 3450 6650
Wire Wire Line
	3450 6650 3900 6650
Wire Wire Line
	3900 6750 3400 6750
Wire Wire Line
	3400 6750 3400 8550
Wire Wire Line
	3400 8550 7600 8550
Wire Wire Line
	6400 5400 7100 5400
Wire Wire Line
	7100 5400 7100 5200
Wire Wire Line
	7300 5200 7300 5450
Wire Wire Line
	8300 7600 8300 7500
Wire Wire Line
	8300 7500 8400 7500
Wire Wire Line
	8300 4700 8300 4600
Wire Wire Line
	8300 4600 8400 4600
Wire Wire Line
	8400 4400 8250 4400
Wire Wire Line
	8250 4400 8250 4000
Wire Wire Line
	8250 4000 6650 4000
Wire Wire Line
	6650 4000 6650 4300
Wire Wire Line
	6600 4500 6600 3950
Wire Wire Line
	6600 3950 8200 3950
Wire Wire Line
	8200 3950 8200 4500
Wire Wire Line
	8200 4500 8400 4500
Wire Wire Line
	2600 9200 3000 9200
Wire Wire Line
	2700 9500 2700 9400
Wire Wire Line
	2700 9400 2600 9400
Wire Wire Line
	3400 9500 3400 9600
Wire Wire Line
	3900 9100 3900 9300
Wire Wire Line
	3900 9200 3800 9200
Connection ~ 7850 4700
Wire Wire Line
	7850 5200 7850 5100
Wire Wire Line
	4900 5200 4900 4900
Wire Wire Line
	6050 7300 6050 8800
Wire Wire Line
	7550 7400 8400 7400
Wire Wire Line
	7600 7300 8400 7300
Wire Wire Line
	8400 7200 7450 7200
Wire Wire Line
	7300 7000 8400 7000
Wire Wire Line
	7350 6900 8400 6900
Wire Wire Line
	7400 6800 8400 6800
Wire Wire Line
	3750 8150 7250 8150
Wire Wire Line
	7250 8150 7250 6700
Wire Wire Line
	7250 6700 8400 6700
Wire Wire Line
	7200 8050 7200 6600
Wire Wire Line
	7200 6600 8400 6600
Wire Wire Line
	4500 8100 7150 8100
Wire Wire Line
	7150 8100 7150 6500
Wire Wire Line
	7150 6500 8400 6500
Wire Wire Line
	7100 8200 7100 6400
Wire Wire Line
	7100 6400 8400 6400
Wire Wire Line
	7050 8000 7050 6300
Wire Wire Line
	7050 6300 8400 6300
Wire Wire Line
	5750 7200 7000 7200
Wire Wire Line
	7000 7200 7000 6200
Wire Wire Line
	7000 6200 8400 6200
Wire Wire Line
	5750 7100 6950 7100
Wire Wire Line
	6950 7100 6950 6100
Wire Wire Line
	6950 6100 8400 6100
Wire Wire Line
	5750 7000 6900 7000
Wire Wire Line
	6900 7000 6900 6000
Wire Wire Line
	6900 6000 8400 6000
Wire Wire Line
	5750 6900 6850 6900
Wire Wire Line
	6850 6900 6850 5900
Wire Wire Line
	6850 5900 8400 5900
Wire Wire Line
	5750 6800 6800 6800
Wire Wire Line
	6800 6800 6800 5800
Wire Wire Line
	6800 5800 8400 5800
Wire Wire Line
	5750 6700 6750 6700
Wire Wire Line
	6750 6700 6750 5700
Wire Wire Line
	6750 5700 8400 5700
Wire Wire Line
	5750 6100 6700 6100
Wire Wire Line
	6700 6100 6700 5600
Wire Wire Line
	6700 5600 8400 5600
Wire Wire Line
	6250 7800 6250 7900
Wire Wire Line
	6250 7900 6050 7900
Connection ~ 6050 7900
Wire Wire Line
	6250 7400 6250 7500
Wire Wire Line
	2900 9300 2900 9200
Connection ~ 2900 9200
Wire Wire Line
	2900 9700 2900 9600
Connection ~ 3900 9200
Wire Wire Line
	3900 9700 3900 9600
Wire Wire Line
	5450 5100 5450 5250
Connection ~ 5450 5250
Wire Wire Line
	5300 5100 5300 5200
Connection ~ 5300 5200
Wire Wire Line
	7700 3850 7700 4000
Connection ~ 7700 4000
Wire Wire Line
	7850 3850 7850 4300
Connection ~ 7850 4300
Wire Wire Line
	7500 7100 8400 7100
Wire Wire Line
	7500 8450 7500 7100
Wire Wire Line
	7500 8450 3500 8450
Wire Wire Line
	3500 8450 3500 6950
Wire Wire Line
	3500 6950 3900 6950
Text Label 3750 6650 0    60   ~ 0
34
Text Label 3750 6750 0    60   ~ 0
35
Text Label 3750 6850 0    60   ~ 0
32
Text Label 3750 6950 0    60   ~ 0
33
Text Label 3750 7050 0    60   ~ 0
25
Text Label 3750 7150 0    60   ~ 0
26
Text Label 3750 7250 0    60   ~ 0
27
Text Label 3750 7350 0    60   ~ 0
14
Text Label 3800 7450 0    60   ~ 0
12
Text Label 5750 7200 0    60   ~ 0
4
Text Label 5750 7100 0    60   ~ 0
16
Text Label 5750 7000 0    60   ~ 0
17
Text Label 5750 6900 0    60   ~ 0
5
Text Label 5750 6800 0    60   ~ 0
18
Text Label 5750 6700 0    60   ~ 0
19
Text Label 5750 6500 0    60   ~ 0
21
Text Label 4500 8000 0    60   ~ 0
13
Text Label 5200 8000 0    60   ~ 0
15
Text Label 5350 8000 0    60   ~ 0
2
Text Label 5750 6100 0    60   ~ 0
37
$EndSCHEMATC
