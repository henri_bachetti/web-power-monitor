#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os, locale, logging
from datetime import datetime
from dateutil import parser
import cherrypy
from cherrypy.lib.cptools import serve_file; 
from template import Template

logger = logging.getLogger('index')

current_dir = os.path.realpath(os.path.curdir)

gpioSerie = """
        {
          name: '%s',
          type: 'spline',
          data: [%s],
        },
		"""

adcSerie = """
        {
          name: '%s',
          type: 'spline',
          data: [%s],
          tooltip: {
              valueSuffix: 'V'
          },
        },
		"""

class HomePage(object):

	def __init__(self):
		logger.setLevel(logging.DEBUG)
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		ch.setFormatter(formatter)
		logger.addHandler(ch)
		self.data = {}
		self.xLabel = 's'

	def load(self, fileName):
		f = open(fileName, 'r')
		return f.read()

	def getXData(self):
		data = ''
		k = self.data.keys()
		k.sort()
		for dt in k:
			if len(data) <> 0:
				data += ', '
			data += "Date.UTC(%s, %s, %s, %s, %s, %s, %s)" % (dt.year, dt.month-1, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond/1000)
			
		return data

	def hasPowerData(self):
		k = self.data.keys()
		k.sort()
		if len(k) and 'CURRENT' in self.data[k[0]]:
			return True
		return False

	def hasGpioData(self):
		k = self.data.keys()
		k.sort()
		if len(k):
			gpioList = self.data[k[0]]
			for gpio in gpioList:
				serie = ''
				if gpio.startswith('GPIO'):
					return True
		return False

	def hasAdcData(self):
		k = self.data.keys()
		k.sort()
		if len(k):
			adcList = self.data[k[0]]
			for adc in adcList:
				serie = ''
				if adc.startswith('ADC'):
					return True
		return False
		
	def getCurrentData(self):
		data = ''
		k = self.data.keys()
		k.sort()
		if len(k) and 'CURRENT' in self.data[k[0]]:
			for entry in k:
				if len(data) <> 0:
					data += ', '
				data += self.data[entry]['CURRENT']
		return data

	def getVoltageData(self):
		data = ''
		k = self.data.keys()
		k.sort()
		if len(k) and 'VOLTAGE' in self.data[k[0]]:
			for entry in k:
				if len(data) <> 0:
					data += ', '
				data += self.data[entry]['VOLTAGE']
		return data

	def getGpioSeries(self):
		data = ''
		k = self.data.keys()
		k.sort()
		if len(k):
			gpioList = self.data[k[0]]
			for gpio in gpioList:
				serie = ''
				if gpio.startswith('GPIO'):
					id = gpio
					if not gpio[4:].isdigit():
						id = gpio[5:]
					for entry in k:
						if len(serie) <> 0:
							serie += ', '
						serie += self.data[entry][gpio]
					data += gpioSerie % (id, serie)
		return data

	def getAdcSeries(self):
		data = ''
		k = self.data.keys()
		k.sort()
		if len(k):
			adcList = self.data[k[0]]
			for adc in adcList:
				serie = ''
				if adc.startswith('ADC'):
					id = adc
					if not adc[3:].isdigit():
						id = adc[4:]
					for entry in k:
						if len(serie) <> 0:
							serie += ', '
						serie += self.data[entry][adc]
					data += adcSerie % (id, serie)
		return data

	@cherrypy.expose
	def favicon_ico(self):
		current_dir = os.path.realpath(os.path.curdir)
		return serve_file(current_dir + '/favicon.ico')
		 
	@cherrypy.expose
	def index(self):
		tmpl = self.load('index.html')
		nameSpace = {'XDATA': self.getXData(),
					   	'XLABEL': self.xLabel,
					   	'CURRENT_DATA': self.getCurrentData(),
						'VOLTAGE_DATA': self.getVoltageData(),
						'GPIO_SERIES': self.getGpioSeries(),
						'ADC_SERIES': self.getAdcSeries(),
						'DISPLAY_POWER': "block" if self.hasPowerData() else "none",
						'DISPLAY_GPIO': "block" if self.hasGpioData() else "none",
						'DISPLAY_ADC': "block" if self.hasAdcData() else "none"
					}
		return Template(tmpl, searchList=[nameSpace]).respond()

	@cherrypy.expose
	def clear(self):
		self.data = {}
		raise cherrypy.HTTPRedirect("/") 

	@cherrypy.expose
	def export(self, filename='log.csv'):
		k = self.data.keys()
		k.sort()
		data = 'TIME'
		if len(k):
			f = open(filename, 'w')
			columns = self.data[k[0]].keys()
			columns.sort()
			for col in columns:
				data += ';'
				data += col
			data += '\n'
			f.write(data)
			data = ''
			for entry in k:
				data += str(entry)
				for col in columns:
					data += ';'
					data += self.data[entry][col]
				data += '\n'
				f.write(data)
				data = ''
			f.close()
			path = os.path.join(os.getcwd(), filename)
			return serve_file(path, 'application/x-download', 'attachment', os.path.basename(path))

	@cherrypy.expose
	def log(self, **kwargs):
		logger.debug('log %s' % kwargs)
		if "dt" in kwargs:
			dt = parser.parse(kwargs["dt"])
		else:
			dt = datetime.now()
		self.data[dt] = {}
		for arg in kwargs.keys():
			self.data[dt][arg] = kwargs[arg]
		tmpl = self.load('index.html')
		return "OK"

cherrypy.root = HomePage()

if __name__ == '__main__':
	cherrypy.config.update(file = 'web-power-monitor.conf')
	cherrypy.log('SERVER starting at %s' % os.getcwd())
	cherrypy.server.start()
