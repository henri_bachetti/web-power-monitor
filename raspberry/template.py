
import logging

logger = logging.getLogger("index")

class Template(object):

	def __init__(self, template, nameSpace=None, searchList=None):
		self.template = template
		self.nameSpace = nameSpace
		self.searchList = searchList

	def search(self, name):
		if self.searchList:
			for nameSpace in self.searchList:
				if name in nameSpace:
					return nameSpace[name]
		if self.nameSpace:
			for nameSpace in self.nameSpace:
				if name in nameSpace:
					return nameSpace[name]
		return None

	def respond(self):
		text = ''
		found = False
		escape = False
		for i in range(len(self.template)):
			c = self.template[i]
			if found:
				if c.isalnum() or c in '-_':
					name += c
				else:
					if len(name):
						value = self.search(name)
						if value <> None:
							text += str(value)
						else:
							text += '$' + name
					else:
						text += '$'
					text += c
					found = False
			elif c == '\\':
				escape = True
			elif c == '$':
				if escape :
					text += c
					escape = False
				else:
					name = ''
					found = True
			else:
				if escape :
					text += '\\'
					escape = False
				text += c
		return text
