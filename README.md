# WEB POWER MONITOR

The purpose of this page is to explain step by step the realization of a WIFI WEB logger based on ESP32.

It can measure voltages, currents and logic signals.

This logger transmits the measurements to a PYTHON WebServer located on a PC or a RASPBERRY PI.
The results can be visualized using a WEB broser.

The PYTHON software uses the CherryPy WEB framework and JavaScript HiCharts library is used.

The board uses the following components :

 * an ESP32
 * an INA226 module
 * some passive components
 * the board is powered by the USB or an AC adapter

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/01/un-logger-analogique-et-digital-version.html

