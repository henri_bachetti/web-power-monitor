
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <SPIFFS.h>
#include <INA226.h>

#define MAX_GPIO          40
#define VREF              0.975
#define ATTENUATION       3.6

#define INA226_I2CADDR            0x41
#define INA226_OHM                0.105  // ohm
#define INA226_AMP                0.8
#define INA226_OFFSET             0.000   // amps

INA226 ina226;

const char *ssid = "XXXXXXXXXX";
const char *password = "XXXXXXXXXXXXXXXXXXXXXXXXX";

char hostName[20] = "192.168.1.134";
int port;
int started;
int powerMonitor;
int gpioMonitor[MAX_GPIO];
String gpioAlias[MAX_GPIO];
int adcMonitor[MAX_GPIO];
float adcDivider[MAX_GPIO];
unsigned long period;

WebServer server(80);
WiFiClient client;

const int led = 13;

void checkConfig()
{
  Serial.print("Mode:                  ");
  switch (ina226.getMode())
  {
    case INA226_MODE_POWER_DOWN:      Serial.println("Power-Down"); break;
    case INA226_MODE_SHUNT_TRIG:      Serial.println("Shunt Voltage, Triggered"); break;
    case INA226_MODE_BUS_TRIG:        Serial.println("Bus Voltage, Triggered"); break;
    case INA226_MODE_SHUNT_BUS_TRIG:  Serial.println("Shunt and Bus, Triggered"); break;
    case INA226_MODE_ADC_OFF:         Serial.println("ADC Off"); break;
    case INA226_MODE_SHUNT_CONT:      Serial.println("Shunt Voltage, Continuous"); break;
    case INA226_MODE_BUS_CONT:        Serial.println("Bus Voltage, Continuous"); break;
    case INA226_MODE_SHUNT_BUS_CONT:  Serial.println("Shunt and Bus, Continuous"); break;
    default: Serial.println("unknown");
  }

  Serial.print("Samples average:       ");
  switch (ina226.getAverages())
  {
    case INA226_AVERAGES_1:           Serial.println("1 sample"); break;
    case INA226_AVERAGES_4:           Serial.println("4 samples"); break;
    case INA226_AVERAGES_16:          Serial.println("16 samples"); break;
    case INA226_AVERAGES_64:          Serial.println("64 samples"); break;
    case INA226_AVERAGES_128:         Serial.println("128 samples"); break;
    case INA226_AVERAGES_256:         Serial.println("256 samples"); break;
    case INA226_AVERAGES_512:         Serial.println("512 samples"); break;
    case INA226_AVERAGES_1024:        Serial.println("1024 samples"); break;
    default: Serial.println("unknown");
  }

  Serial.print("Bus conversion time:   ");
  switch (ina226.getBusConversionTime())
  {
    case INA226_BUS_CONV_TIME_140US:  Serial.println("140uS"); break;
    case INA226_BUS_CONV_TIME_204US:  Serial.println("204uS"); break;
    case INA226_BUS_CONV_TIME_332US:  Serial.println("332uS"); break;
    case INA226_BUS_CONV_TIME_588US:  Serial.println("558uS"); break;
    case INA226_BUS_CONV_TIME_1100US: Serial.println("1.100ms"); break;
    case INA226_BUS_CONV_TIME_2116US: Serial.println("2.116ms"); break;
    case INA226_BUS_CONV_TIME_4156US: Serial.println("4.156ms"); break;
    case INA226_BUS_CONV_TIME_8244US: Serial.println("8.244ms"); break;
    default: Serial.println("unknown");
  }

  Serial.print("Shunt conversion time: ");
  switch (ina226.getShuntConversionTime())
  {
    case INA226_SHUNT_CONV_TIME_140US:  Serial.println("140uS"); break;
    case INA226_SHUNT_CONV_TIME_204US:  Serial.println("204uS"); break;
    case INA226_SHUNT_CONV_TIME_332US:  Serial.println("332uS"); break;
    case INA226_SHUNT_CONV_TIME_588US:  Serial.println("558uS"); break;
    case INA226_SHUNT_CONV_TIME_1100US: Serial.println("1.100ms"); break;
    case INA226_SHUNT_CONV_TIME_2116US: Serial.println("2.116ms"); break;
    case INA226_SHUNT_CONV_TIME_4156US: Serial.println("4.156ms"); break;
    case INA226_SHUNT_CONV_TIME_8244US: Serial.println("8.244ms"); break;
    default: Serial.println("unknown");
  }

  Serial.print("Max possible current:  ");
  Serial.print(ina226.getMaxPossibleCurrent());
  Serial.println(" A");

  Serial.print("Max current:           ");
  Serial.print(ina226.getMaxCurrent());
  Serial.println(" A");

  Serial.print("Max shunt voltage:     ");
  Serial.print(ina226.getMaxShuntVoltage());
  Serial.println(" V");

  Serial.print("Max power:             ");
  Serial.print(ina226.getMaxPower());
  Serial.println(" W");
}

void mount(void)
{                                                                                                        
  int count;
  size_t total = 0, used = 0;
  struct dirent *ent;

  if (!SPIFFS.begin(true)) {
    printf("SPIFFS mount failed\n");
    return;
  }
  printf("\nSPIFFS mounted\n");
  printf("%ld / %ld bytes used\n", SPIFFS.usedBytes(), SPIFFS.totalBytes());
  File dir = SPIFFS.open("/");
  if (!dir) {
    printf("error opening /\n");
    return;
  }
  File file = dir.openNextFile();
  while (file) {
    printf("%s: %d bytes\n", file.name(), file.size());
    file = dir.openNextFile();
  }
  dir.close();
}

int hasArg(String &name)
{
  for (int i = 0; i < server.args(); i++) {
    if (server.argName(i) == name) {
      return i;
    }
  }
  return -1;
}

void handleRoot()
{
  Serial.println("handleRoot");
  String buf;

  started = false;
  File file = SPIFFS.open("/index.html");
  if (!file) {
    Serial.println("file not found");
    server.send(200, "text/plain", "index.html not found");
    return;
  }
  while (file.available()) {
    buf += file.readStringUntil('\n');
    buf += '\n';
  }
  Serial.println(buf);
  file.close();
  server.send(200, "text/html", buf);
}

void handleMonitor()
{
  int looking = false;

  Serial.println("handleMonitor");
  String buf = "<!DOCTYPE HTML><html>\n<head>"
               "  <style>"
               "    body {"
               "      min-width: 310px;"
               "      max-width: 800px;"
               "      height: 400px;"
               "      margin: 0 auto;"
               "    }"
               "    h2 {"
               "      font-family: Arial;"
               "      font-size: 2.5;"
               "      text-align: center;"
               "    }"
               "  </style>"
               "</head>"
               "<body>"
               "  <h2>WEB Power Monitor RUNNING</h2>";

  started = true;
  powerMonitor = false;
  memset(gpioMonitor, 0, sizeof(gpioMonitor));
  memset(gpioAlias, 0, sizeof(gpioAlias));
  memset(adcMonitor, 0, sizeof(adcMonitor));
  for (int i = 0; i < server.args(); i++) {
    Serial.print(server.argName(i) + " ");
    Serial.println(server.arg(i));
    if (server.argName(i) == "host") {
      server.arg(i).toCharArray(hostName, 20);
      buf += "Host: " + server.arg(i) + "<br>";
    }
    else if (server.argName(i) == "port") {
      port = server.arg(i).toInt();
      buf += "Port: " + server.arg(i) + "<br>";
    }
    if (server.argName(i) == "period") {
      period = server.arg(i).toInt();
    }
    else if (server.argName(i) == "unit") {
      String unit = " Milliseconds";
      long per = period;
      if (server.arg(i) == "m") {
        period *= 60000;
        unit = " Minutes";
      }
      else if (server.arg(i) == "s") {
        period *= 1000;
        unit = " Seconds";
      }
      buf += "Period: " + String(per) + unit + "<br><br>";
    }
    else if (server.argName(i) == "power") {
      if (!looking) {
        buf += "Looking for:<br>";
        looking = true;
      }
      powerMonitor = true;
      buf += "- Power Monitor<br>";
    }
    else if (server.argName(i).startsWith("gpioname_")) {
      int gpioNumber = server.argName(i).substring(9).toInt();
      if (server.arg(i).length()) {
        gpioAlias[gpioNumber] = server.arg(i);
      }
    }
    else if (server.argName(i).startsWith("gpio")) {
      if (!looking) {
        buf += "Looking for:<br>";
        looking = true;
      }
      int gpioNumber = server.arg(i).toInt();
      gpioMonitor[gpioNumber] = true;
      pinMode(gpioNumber, INPUT_PULLUP);
      int arg = hasArg(String("gpioname_") + server.arg(i));
      if (server.arg(arg).length()) {
        buf += "- " + server.arg(arg) + " (" + server.argName(i) + ")<br>";
      }
      else {
        buf += "- " + server.argName(i) + "<br>";
      }
    }
    else if (server.argName(i).startsWith("adcname_")) {
      int gpioNumber = server.argName(i).substring(8).toInt();
      if (server.arg(i).length()) {
        gpioAlias[gpioNumber] = server.arg(i);
      }
    }
    else if (server.argName(i).startsWith("adcdivider_")) {
      int gpioNumber = server.argName(i).substring(11).toInt();
      if (server.arg(i).length()) {
        adcDivider[gpioNumber] = server.arg(i).toFloat();
      }
    }
    else if (server.argName(i).startsWith("adc")) {
      if (!looking) {
        buf += "Looking for:<br>";
        looking = true;
      }
      adcMonitor[server.arg(i).toInt()] = true;
      int nameArg = hasArg(String("adcname_") + server.arg(i));
      int divArg = hasArg(String("adcdivider_") + server.arg(i));
      if (server.arg(nameArg).length()) {
        buf += "- " + server.arg(nameArg) + " (" + server.argName(i) + ", divider=" + server.arg(divArg) + ")<br>";
      }
      else {
        buf += "- " + server.argName(i) + " (divider=" + server.arg(divArg) + ")<br>";
      }
    }
  }
  server.send(200, "text/html", buf);
}

void handleNotFound()
{
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void)
{
  pinMode(led, OUTPUT);
//  analogSetAttenuation(ADC_6db);
  digitalWrite(led, 0);
  Serial.begin(115200);
  ina226.begin(INA226_I2CADDR);
  ina226.configure(INA226_AVERAGES_16, INA226_BUS_CONV_TIME_8244US, INA226_SHUNT_CONV_TIME_8244US, INA226_MODE_SHUNT_BUS_CONT);
  ina226.calibrate(INA226_OHM, INA226_AMP);
  checkConfig();
  mount();
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
  if (MDNS.begin("esp32")) {
    Serial.println("MDNS responder started");
  }

  server.on("/index.html", handleRoot);
  server.on("/monitor.html", handleMonitor);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void)
{
  String gpio, adc;
  String url = "/log?";
  unsigned long currentMillis = millis();
  static unsigned long previousMillis;

  server.handleClient();
  if (started && currentMillis - previousMillis > period) {
    previousMillis = currentMillis;
    if (powerMonitor) {
      float voltage = ina226.readBusVoltage();
      float current = (ina226.readShuntCurrent() + INA226_OFFSET) * 1000;
      Serial.print("INA226: ");
      Serial.print(voltage, 3);
      Serial.print("V, ");
      Serial.print(current, 3);
      Serial.println("mA");
      url += String("VOLTAGE=") + voltage + "&CURRENT=" + current + "&";
    }
    for (int i = 0 ; i < MAX_GPIO ; i++) {
      if (gpioMonitor[i]) {
        gpio = "GPIO" + String(i);
        int state = digitalRead(i);
        if (gpioAlias[i].length()) {
          url += String("GPIO_") + gpioAlias[i] + "=" + state + "&";
        }
        else {
          url += gpio + "=" + state + "&";
        }
      }
      if (adcMonitor[i]) {
        adc = "ADC" + String(i);
        int value = analogRead(i);
        Serial.print("ADC: ");
        Serial.println(value);
        float voltage;
        Serial.print("DIVIDER: ");
        Serial.println(adcDivider[i]);
        if (adcDivider[i]) {
          voltage = VREF * value / 4095 * ATTENUATION * adcDivider[i];
        }
        else {
          voltage = VREF * value / 4095 * ATTENUATION;
        }
        if (gpioAlias[i].length()) {
          url += String("ADC_") + gpioAlias[i] + "=" + voltage + "&";
        }
        else {
          url += adc + "=" + voltage + "&";
        }
      }
    }
    String data = String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + hostName + "\r\n" + "Connection: close\r\n\r\n";
    Serial.println(data);
    Serial.print(hostName);
    Serial.print(":");
    Serial.print(port);
    if (client.connect(hostName, port)) {
      Serial.println(": connected to host");
      Serial.println("Sending data");
      client.println(data);
    }
    else {
      Serial.println(": unable to connect to host");
    }
  }
}


